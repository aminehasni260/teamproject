package team.demo;

public interface customerService {
	public customer Create           (customer c);
	public customer ReadById         (customer c);
	public customer ReadByAll        (customer c);
	public customer Update           (customer c);
	public void DeleteByid           ();
	public void DeleteByCustomer     ();

}
