package team.demo;

public class customer {
	private long id;
	private String fullName ;
	private Integer age ;
	private Boolean isStudent ;
	
	public customer(long id, String fullName, Integer age, Boolean isStudent) {
		super();
		this.id = id;
		this.fullName = fullName;
		this.age = age;
		this.isStudent = isStudent;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public Integer getAge() {
		return age;
	}
	public void setAge(Integer age) {
		this.age = age;
	}
	public Boolean getIsStudent() {
		return isStudent;
	}
	public void setIsStudent(Boolean isStudent) {
		this.isStudent = isStudent;
	}
	

}
