package team.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoTeamApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoTeamApplication.class, args);
	}

}
